﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Special_Characters
{
    class Program
    {
        private static CharacterDatabase Database = new CharacterDatabase();
        
        static void Main(string[] args)
        {
            string charcters = null;
            Console.WriteLine("write a character: ");
            var input = Console.ReadLine();
            foreach (char specialcharacters in input)
            {
                var query = from character in Database.CharacterList()
                             select character;

                //var dbChar = from character in Database.CharacterList()
                            //select character.Character;

                foreach (var character in query)
                {
                    if (specialcharacters == character.Character)
                    {
                        
                        charcters += character.Number;
                    }
                }
            }
            Console.WriteLine(charcters);
            while (charcters == null || charcters.Length < input.Length)
            {
                Console.WriteLine("Wrong Character");
                Console.ReadLine();
            }
            return;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Special_Characters
{
    public class CharacterDatabase
    {
        public IEnumerable<CharacterModel> CharacterList()
        {

            return new List<CharacterModel>()
            {
                new CharacterModel{ID = 1, Number = "0", Character = ')'},
                new CharacterModel{ID = 2, Number = "1", Character = '('},
                new CharacterModel{ID = 3, Number = "2", Character = '*'},
                new CharacterModel{ID = 4, Number = "3", Character = '&'},
                new CharacterModel{ID = 5, Number = "4", Character = '^'},
                new CharacterModel{ID = 6, Number = "5", Character = '%'},
                new CharacterModel{ID = 7, Number = "6", Character = '$'},
                new CharacterModel{ID = 8, Number = "7", Character = '$'},
                new CharacterModel{ID = 9, Number = "8", Character = '@'},
                new CharacterModel{ID = 10, Number = "9", Character = '!'},
            };
        }
    }
}

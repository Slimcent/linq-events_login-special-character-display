﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Special_Characters
{
    public class CharacterModel
    {
        public int ID { get; set; }
        public string Number { get; set; }
        public char Character { get; set; }
    }
}

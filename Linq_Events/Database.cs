﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Linq_Events
{
    public class Database
    {
        public IEnumerable<Bezao> Genesys()
        {

            return new List<Bezao>()
            {
                new Bezao{ID = 1, Name = "Chinese Chikky"},
                new Bezao{ID = 2, Name = "Scientific Sammy"},
                new Bezao{ID = 3, Name = "Strategic Sammy"},
                new Bezao{ID = 4, Name = "calculative Chinedu"},
                new Bezao{ID = 5, Name = "sincere Shola "},
                new Bezao{ID = 6, Name = "Terrible Tochukwu"},
                new Bezao{ID = 7, Name = "Daring Dara"},
                new Bezao{ID = 8, Name = "Underrated Uriel"},
                new Bezao{ID = 9, Name = "Adventurous Alex"},
                new Bezao{ID = 10, Name = "Logical Loveth"},
                new Bezao{ID = 11, Name = "Knowledgeable Kc"},
                new Bezao{ID = 12, Name = "Friendly Francis"},
                new Bezao{ID = 13, Name = "Kinetic Kachi"},
                new Bezao{ID = 14, Name = "Simple Sunday"},
                new Bezao{ID = 15, Name = "Optimistic Obinna"},
                new Bezao{ID = 16, Name = "Generous Gideon"},
            };
        }
    }
}

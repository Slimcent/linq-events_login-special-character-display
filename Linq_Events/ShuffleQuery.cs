﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Linq_Events
{
    public static class ShuffleQuery
    {
        private static Database Database = new Database();
        public static void Shuffle()
        {
            // Shuffle in Query Syntax
            Random rd = new Random();
            var query = from Bezao in Database.Genesys()
                              orderby rd.Next()
                              select Bezao.Name;
            Console.WriteLine(".........Shuffle in Query Syntax.............");
            foreach (var dev in query)
            {
                Console.WriteLine(dev);
            }
            

            // Shuffle in Method Syntax
            var rnd = new Random();
            var rsult = Database.Genesys().OrderBy(d => rnd.Next());
            Console.WriteLine("\n.........Shuffle in Method Syntax..........");
            foreach (var dev in rsult)
            {
                Console.WriteLine(dev.Name);
            }


            // Full list in Database in Ascending
            var method = from Bezao in Database.Genesys()
                                  orderby Bezao.Name ascending
                                  select Bezao.Name;
            Console.WriteLine("\n..........Full list in Ascending Order............");
            foreach (var dev in method)
            {
                Console.WriteLine(dev);
            }
            Console.ReadLine();

        }

    }
}

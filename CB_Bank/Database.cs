﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CB_Bank
{
    public class Database
    {
        public IEnumerable<Users> Genesys()
        {

            return new List<Users>()
            {
                new Users{ID = 1, Name = "Chikky", AccountNumber = 30379928020, BVN = 0011223344, Password = "1234"},
                new Users{ID = 2, Name = "Sammy", AccountNumber = 30370928088, BVN = 0001223344, Password = "1238"},
                new Users{ID = 3, Name = "Jane", AccountNumber = 30370928066, BVN = 0019923344, Password = "1277"},
                new Users{ID = 4, Name = "Chinedu", AccountNumber = 30388928020, BVN = 8881223344, Password = "1007"},
                new Users{ID = 5, Name = "Bill", AccountNumber = 30370028020, BVN = 00118823344, Password = "8734"},
                new Users{ID = 6, Name = "James", AccountNumber = 30990928020, BVN = 0881223344, Password = "1004"},
                new Users{ID = 7, Name = "Dara", AccountNumber = 30370928000, BVN = 0077223344, Password = "1776"},
                new Users{ID = 8, Name = "Uriel", AccountNumber = 30370977720, BVN = 0991223344, Password = "1094"},
                new Users{ID = 9, Name = "Alex", AccountNumber = 30370924567, BVN = 0661223344, Password = "0923"},
                new Users{ID = 10, Name = "Loveth", AccountNumber = 36780928020, BVN = 0099223344, Password = "1834"},
                new Users{ID = 11, Name = "Kc", AccountNumber = 30888928020, BVN = 0881223344, Password = "0094"},
                new Users{ID = 12, Name = "Francis", AccountNumber = 39970928020, BVN = 0011223344, Password = "0894"},
                new Users{ID = 13, Name = "Kachi", AccountNumber = 30399728020, BVN = 0018823344, Password = "1990"},
                new Users{ID = 14, Name = "Sunday", AccountNumber = 30300928020, BVN = 0001223344, Password = "1901"},
                new Users{ID = 15, Name = "Obinna", AccountNumber = 30370928020, BVN = 0761223344, Password = "0230"},
                new Users{ID = 16, Name = "Gideon", AccountNumber = 30990927689, BVN = 0971223344, Password = "9232"},
            };
        }
    }
}

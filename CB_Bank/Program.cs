﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CB_Bank.Subscriptions;
namespace CB_Bank
{
    class Program
    {
        static void Main(string[] args)
        {
            var msg = new Message()
            {
                Title = "Notificaton sent",
            };
            var bank = new Bank(); // publisher
            var mailService = new MailService(); //subscriber
            var message = new MessageService();

            bank.LoginMonitor += mailService.Mailsender;
            bank.LoginMonitor += message.Messagesender;
            bank.EncodeMessage(msg);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CB_Bank
{
    public class NotificationEventArgs : EventArgs
    {
        public Message Message { get; set; }
    }
    public class Bank
    {
        Database database = new Database();
        public event EventHandler<NotificationEventArgs> LoginMonitor; // Event Handler
       
        public void EncodeMessage(Message message)
        {
            Tuple<string, string, string> blackList;
            blackList = new Tuple<string, string, string>("Jane", "Bill", "James");
            Console.WriteLine("Monitoring Login Process");
            Thread.Sleep(2000);
            Console.WriteLine("Enter your Password");
            var password = Console.ReadLine();
            while (string.IsNullOrEmpty(password))
            {
                Validation.InvlidPrompt("Password");
                password = Console.ReadLine();
                Console.ResetColor();
            }
            var querySyntax = from user in database.Genesys()
                              where user.Password.Equals(password)
                              select  user.Name;
            while (querySyntax.Count() == 0)
            {
                Console.WriteLine("No match found!");
                password = Console.ReadLine();
            }
            foreach (var dev in querySyntax)
            {
                if (dev.Contains(blackList.Item1.ToString()) || dev.Contains(blackList.Item2.ToString())
                        || dev.Contains(blackList.Item3.ToString()))
                {
                Console.WriteLine("Access denied"); 
                OnLoginMonitor(message);
                Validation.PromptAlarm();
                }
                else
                {
                    var uname = dev;
                    Console.WriteLine($"Welcome {uname}");
                    Console.WriteLine("\n1 Check your BVN \n2 Enroll for BVN");
                    var choice = Console.ReadLine();
                    while (choice != "1" && choice != "2")
                    {
                        Console.WriteLine("Invalid choice selection");
                        choice = Console.ReadLine();
                    }
                    Choice(choice);
                    Console.WriteLine( "Hold on, processing option");
                    Console.ReadLine();
                }                  
            }  
        }

        protected virtual void OnLoginMonitor(Message message)
        {
            if (LoginMonitor != null)
                LoginMonitor(this, new NotificationEventArgs() { Message = message });
        }

        public static void Choice(string choice)
        {
            switch (choice)
            {
                case "1":
                    Login.BvnChecking();
                    break;
                case "2":
                    Login.BvnEnrolling();
                    break;
                default:
                    Console.WriteLine("Invalid selection");
                    break;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CB_Bank
{
    public static class Validation
    {
        public static void InvlidPrompt(string field)
        {
            Console.BackgroundColor = ConsoleColor.White;
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine($"{field} is invald, Re-enter your {field}");
        }

        public static void ContainsNumber(string field)
        {
            Console.BackgroundColor = ConsoleColor.White;
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine($"{field} cannot contain nmbers, Re-enter your {field}");
        }

        public static void PromptAlarm()
        {
            int n = 6;
            for (int i = 1; i < n; i++)
            {
                Console.Beep();
            }
        }
    }
}

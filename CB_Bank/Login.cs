﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CB_Bank
{
    public class Login
    {
        Database user = new Database();
        public static void logUser(Users u)
        {
            Console.WriteLine($"Welcome {u.Name} \n1 Check your BVN \n2 Enroll for BVN");
        }

        public static void BvnChecking(Users u)
        {
            Console.WriteLine($"{u.Name}, your BVN is {u.BVN}");
        }

        public static void BvnEnrolling(Users u)
        {
            Console.WriteLine("Enrolling for BVN");
            Thread.Sleep(3000);
            Random rnd = new Random();
            int newBvn = rnd.Next(11);
            u.BVN = newBvn;
            Console.WriteLine($"Your new BVN is {u.BVN}");
        }
    }
}
